import React, {useState, useEffect, useRef } from 'react';
import { Layout, Menu, Input, Modal } from 'antd';
import './App.css';
import 'antd/dist/antd.css';
import { UserOutlined } from '@ant-design/icons';
import Message from './components/Message';
import io, { Socket } from "socket.io-client";
import { MessageProps, UserProps } from './shared/types';
const { Search } = Input;
const { Header, Sider, Content } = Layout;

const SOCKET_HOST = 'https://cgnt-demo.freemyip.com'
const RICK_AND_MORTY_API = 'https://rickandmortyapi.com/api/character?status=alive&species=human'

function App() {
  const socketRef = useRef<Socket | null>(null);
  const [messages, setMessages] = useState([]);
  const [channels, setChannels] = useState<any[]>([]);
  const [message, setMessage] = useState<string>("");
  const [newChannelName, setNewChannelName] = useState<string>("");
  const [currentChannel, setCurrentChannel] = useState<string>("");
  const [userModalOpen, setUserModalOpen] = useState<boolean>(false);
  const [channelModalOpen, setChannelModalOpen] = useState<boolean>(false);
  const [currentUser, setCurrentUser] = useState<UserProps>({} as UserProps);
  const [characters, setCharacters] = useState<any[]>([]);
  const [channelMessages, setChannelMessages] = useState<{ [key: string]: string; }>({})

  useEffect(() => {
    const socket = io(SOCKET_HOST);
    socket.emit('showChannels')
    socket.on('listMessages', listMessages => {
      console.log('lmsgs: ',listMessages)
      listMessages.draftMessage ? setMessage(listMessages.draftMessage) : setMessage('')
      setMessages(listMessages.messages.reverse())
    })
    socket.on('listChannels', listChannels => setChannels(listChannels))
    socketRef.current = socket;

    fetch(RICK_AND_MORTY_API)
      .then(response => response.json())
      .then(data => setCharacters(data.results.slice(0,9)) );
  }, [])

  useEffect(() => {
    if(Object.keys(currentUser).length === 0){
      setUserModalOpen(true);
    }
  }, [currentUser])

  useEffect(() => {
    if(socketRef.current){
      if(channels.length > 0 && currentChannel === ""){
        setCurrentChannel(channels[0].channelId)
        socketRef.current.emit('joinChannel', {channel: channels[0].channelId})
      } else {
        socketRef.current.emit('joinChannel', {channel: currentChannel})
      }
    }
  }, [channels, currentChannel])

  useEffect(() => {
    if(characters.length > 0){
      setCurrentUser(characters[0])
    }
  }, [characters])

  useEffect(() => {
    if(Object.keys(channelMessages).includes(currentChannel)){
      setMessage(channelMessages[currentChannel])
    } else {
      setMessage("")
    }
  }, [currentChannel])

  useEffect(() => {
    if(socketRef.current){
      socketRef.current.emit('setChannelDraft', { message, currentChannel })
    }
    setChannelMessages({...channelMessages,[currentChannel]: message})
  }, [message])

  const sendMessage = () => {
    const { name, image } = currentUser;
    if(socketRef.current){
      socketRef.current.emit('sendMessage', {message, channelId: currentChannel, name, image})
      setMessage("")
    }
  }

  const handleMenuChange = (menu: any) => {
    const { key } = menu;
    if(key !== "new-channel"){
      if(key !== currentChannel && socketRef.current){
        socketRef.current.emit('leaveChannel', {channel: currentChannel})
        socketRef.current.emit('joinChannel', {channel: key})
        setCurrentChannel(menu.key)
      }
    } else {
      setChannelModalOpen(true)
    }
  }

  const createChannel = () => {
    if(socketRef.current){
      socketRef.current.emit('createChannel', {name: newChannelName })
      setNewChannelName("")
      setChannelModalOpen(false)
    }
  }

  const switchUser = (user: UserProps) => {
    setCurrentUser(user);
    setUserModalOpen(false);
  }

  return (
      <Layout className="main-layout">

      <Sider trigger={null}>
        <div className="logo" />

        <Menu 
          theme="dark" 
          mode="inline" 
          defaultSelectedKeys={[currentChannel]}
          selectedKeys={[currentChannel]}
          onClick={handleMenuChange}
        >

          {channels.map((channel: any) => (
            <Menu.Item key={channel.channelId}>
              #{channel.name}
            </Menu.Item>
          ))}
          
          <Menu.Item key="new-channel">
            New Channel
          </Menu.Item>

        </Menu>
      </Sider>

      <Layout className="site-layout">
        <Header className="site-layout-background header" style={{ padding: 0 }}>
          <div></div>
          <div className="userProfile" onClick={() => setUserModalOpen(true)}>
            <h3>Change User <UserOutlined /></h3>
          </div>
        </Header>

        <Content
          className="site-layout-background"
          style={{
            paddingLeft: 20,
            paddingRight: 20,
            minHeight: 280,
          }}
        >
          <div className="message-layout">

          <div className="messages">

            {messages.map((msg: MessageProps, idx: number) => (
              <Message
                name={msg.name}
                avatar={msg.avatar}
                message={msg.message}
                date={msg.date}
                key={idx}
              />
            ))}

          </div>

          
          <Search
            placeholder="Type a message here."
            allowClear
            enterButton="Send"
            size="large"
            value={message}
            onChange={(e: any) => setMessage(e.target.value)}
            onSearch={sendMessage}
          />

          </div>
        </Content>
      </Layout>


      <Modal
        title="Set your user." 
        visible={userModalOpen} 
        footer={null}
        onCancel={() => setUserModalOpen(false)}
      >
        <div className="characters">
        {characters && characters.map((character: any) => (
          <div 
            className="character" 
            key={character.id} 
            onClick={() => switchUser(character)}
          >
            <img src={character.image} alt=""/>
            <p>{character.name}</p>
          </div>
        ))}
        </div>
      </Modal>

      <Modal
        title="Create a new channel."
        visible={channelModalOpen}
        onOk={createChannel}
        onCancel={() => setChannelModalOpen(false)}
        okText="Create"
      >
        <Input
          placeholder="Channel name"
          onChange={(e: any) => setNewChannelName(e.target.value)}
          value={newChannelName}
        />
      </Modal>

    </Layout>
  );
}

export default App;
