import { MessageProps } from '../../shared/types';

const Message = (message: MessageProps) => {
    return (
    <div className="message">
        <img src={message.avatar} alt=""/>
        <div className="message-data">
            <h3>{message.name}</h3>
            <p>{message.message}</p>
        </div>
    </div>
    )
}

export default Message;