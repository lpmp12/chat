import React from 'react';

export type MessageProps = {
    name: string;
    message: string;
    date: string;
    avatar: string;
};

export type UserProps = {
    id: String;
    name: String;
    image: String;
}